package com.example.tasktodo.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

fun hasInternet(context: Context): Boolean {
    val connectivityManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val activeNetwork = connectivityManager.activeNetwork
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork)

        if (capabilities != null) {
            return if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) true
            else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) true
            else capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
        }
    } else {
        return try {
            val networkInfo = connectivityManager.activeNetworkInfo
            networkInfo != null && networkInfo.isConnected
        } catch (e: java.lang.Exception) {
            false
        }
    }
    return false
}