package com.example.tasktodo.domain.repository

import com.example.tasktodo.TodoItem
import com.example.tasktodo.domain.model.ToDo
import kotlinx.coroutines.flow.Flow

interface TodoRepository {

    fun getAllTodos(): Flow<List<ToDo>>

    suspend fun addTodos(list: List<ToDo>)

    suspend fun updateCompleted(value: Boolean, todoItem: ToDo, index: Int)

}