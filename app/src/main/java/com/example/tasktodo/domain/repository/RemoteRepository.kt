package com.example.tasktodo.domain.repository

import com.example.tasktodo.domain.model.ToDo

interface RemoteRepository {

    suspend fun pushTodos(list: List<ToDo>): String

    suspend fun pullTodos(): List<ToDo>

}