package com.example.tasktodo.domain.usecases

import android.content.Context
import android.util.Log
import androidx.compose.runtime.collectAsState
import com.example.tasktodo.domain.model.ToDo
import com.example.tasktodo.domain.repository.RemoteRepository
import com.example.tasktodo.domain.repository.TodoRepository
import com.example.tasktodo.util.hasInternet
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class GetDataUseCase @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val todoRepository: TodoRepository,
    @ApplicationContext
    private val context: Context
) {

    suspend operator fun invoke(): List<ToDo> {
        return if (hasInternet(context)) {
            remoteRepository.pullTodos()
        } else {
            todoRepository.getAllTodos().first()
        }
    }

}