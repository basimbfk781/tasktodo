package com.example.tasktodo.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tasktodo.TodoItem

@Entity
data class ToDo(
    val todoTitle: String,
    val todoDescription: String,
    var completed: Boolean = false,
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null
) {
    fun toTodoItem(): TodoItem {
        return TodoItem.newBuilder().setTodoTitle(todoTitle).setTodoDescription(todoDescription).setCompleted(completed).build()
    }
}
