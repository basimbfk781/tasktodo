package com.example.tasktodo.presentation.main.home.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Checkbox
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import com.example.tasktodo.TodoItem
import com.example.tasktodo.domain.model.ToDo
import com.example.tasktodo.presentation.main.home.viewmodel.HomeViewModel

@Composable
fun SingleTodo(
    viewModel: HomeViewModel,
    index: Int,
    todoItem: ToDo
) {

    val completedState = remember {
        mutableStateOf(todoItem.completed)
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 24.dp)
    ) {

        Checkbox(checked = completedState.value, onCheckedChange = {
            completedState.value = !completedState.value
            todoItem.completed = completedState.value
            viewModel.updateCompleted(
                todoItem = todoItem,
                value = completedState.value,
                index = index
            )
        })

        Spacer(modifier = Modifier.width(16.dp))

        Column {
            Text(text = todoItem.todoTitle, fontWeight = FontWeight.Bold, textDecoration = if (completedState.value) TextDecoration.LineThrough else TextDecoration.None)
            Text(text = todoItem.todoDescription, fontWeight = FontWeight.Light,  textDecoration = if (completedState.value) TextDecoration.LineThrough else TextDecoration.None)
        }

    }

}