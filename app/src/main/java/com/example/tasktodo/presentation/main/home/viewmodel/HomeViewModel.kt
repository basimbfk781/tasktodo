package com.example.tasktodo.presentation.main.home.viewmodel

import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tasktodo.TodoItem
import com.example.tasktodo.domain.model.ToDo
import com.example.tasktodo.domain.repository.RemoteRepository
import com.example.tasktodo.domain.repository.TodoRepository
import com.example.tasktodo.domain.usecases.GetDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val todoRepository: TodoRepository,
    private val remoteRepository: RemoteRepository,
    private val getDataUseCase: GetDataUseCase
): ViewModel() {

    var state by mutableStateOf(emptyList<ToDo>())
        private set

    var pushState by mutableStateOf("")
        private set

    init {
        getAllTodos()
    }

    private fun getAllTodos() = viewModelScope.launch {
        state = getDataUseCase()
    }
//    private fun getAllTodos() = viewModelScope.launch {
//        todoRepository.getAllTodos().collectLatest {
//            state = it
//        }
//    }

    fun saveAllTodos(list: List<ToDo>) = viewModelScope.launch {
        todoRepository.addTodos(list = list)
    }

    fun updateCompleted(value: Boolean, index: Int, todoItem: ToDo) = viewModelScope.launch {
        todoRepository.updateCompleted(value, todoItem, index)
    }

    fun getAllTodoRemote() = viewModelScope.launch {
        state = remoteRepository.pullTodos()
    }

    fun pushAllTodoRemote(list: List<ToDo>) = viewModelScope.launch {
        pushState = remoteRepository.pushTodos(list)
    }


}