package com.example.tasktodo.presentation.main.home

import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.tasktodo.R
import com.example.tasktodo.domain.model.ToDo
import com.example.tasktodo.presentation.main.home.components.SingleTodo
import com.example.tasktodo.presentation.main.home.viewmodel.HomeViewModel
import kotlinx.coroutines.launch


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreen(
    viewModel: HomeViewModel = hiltViewModel()
) {

    val state = viewModel.state
    
    val sheetState = rememberBottomSheetState(initialValue = BottomSheetValue.Collapsed)
    val scope = rememberCoroutineScope()
    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
        bottomSheetState = sheetState
    )
    val dropdownState = remember {
        mutableStateOf(false)
    }

    val context = LocalContext.current

    LaunchedEffect(key1 = viewModel.pushState) {
        Toast.makeText(context, viewModel.pushState, Toast.LENGTH_LONG).show()
    }

    BottomSheetScaffold(
        scaffoldState = bottomSheetScaffoldState,
        sheetContent = {
           BottomSheetContent(sheetState = sheetState, viewModel = viewModel)
        },
        sheetPeekHeight = 0.dp,
        sheetShape = RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp),
        floatingActionButton = {
            AnimatedVisibility (visible = sheetState.isCollapsed && !sheetState.isAnimationRunning) {
                IconButton(
                    onClick = {
                        scope.launch {
                            sheetState.expand()
                        }
                    },
                    modifier = Modifier
                        .padding(bottom = 72.dp)
                        .clip(shape = CircleShape)
                        .background(Color.Yellow)

                ) {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = null
                    )
                }
            }
        },
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Todos List")
                },
                actions = {

                    IconButton(onClick = { dropdownState.value = !dropdownState.value }) {
                        Icon(imageVector = Icons.Default.MoreVert, contentDescription = null)
                    }
                    DropdownMenu(expanded = dropdownState.value, onDismissRequest = { dropdownState.value = false }) {
                        DropdownMenuItem(onClick = {
                            viewModel.pushAllTodoRemote(state)
                        }) {
                            Text(text = "Push Todos")
                        }

                        DropdownMenuItem(onClick = { viewModel.getAllTodoRemote() }) {
                            Text(text = "Pull Todos")
                        }

                    }
                }
            )
        },
        backgroundColor = colorResource(id = R.color.background)
    ) {

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .clickable {
                    scope.launch {
                        sheetState.collapse()
                    }
                }
        ) {
            
            items(state.size) {
                SingleTodo(viewModel = viewModel, index = it, todoItem = state[it])
            }

        }

    }

}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BottomSheetContent(
    sheetState: BottomSheetState,
    viewModel: HomeViewModel
) {

    val scope = rememberCoroutineScope()

    val todoTitle = remember {
        mutableStateOf("")
    }

    val todoDescription = remember {
        mutableStateOf("")
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(300.dp)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Text(
            text = "Enter your new Todo",
            modifier = Modifier
                .align(
                    alignment = Alignment.Start
                )
                .padding(horizontal = 16.dp),
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp
        )
        Spacer(modifier = Modifier.height(16.dp))

        TextField(value = todoTitle.value, onValueChange = {
            todoTitle.value = it
        },
        placeholder = {
            Text(text = "Title")
        })

        Spacer(modifier = Modifier.height(16.dp))

        TextField(value = todoDescription.value, onValueChange = {
            todoDescription.value = it
        },
        placeholder = {
            Text(text = "Description")
        })

        Spacer(modifier = Modifier.height(16.dp))

        Button(onClick = {
            scope.launch {
                val list = viewModel.state.toMutableList()
                list.add(ToDo(todoTitle = todoTitle.value, todoDescription = todoDescription.value, completed = false))
                viewModel.saveAllTodos(list)
                todoTitle.value = ""
                todoDescription.value = ""
                sheetState.collapse()
            }
        }) {
            Text(text = "Save")
        }
    }
}