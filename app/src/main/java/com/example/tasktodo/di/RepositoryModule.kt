package com.example.tasktodo.di

import com.example.tasktodo.data.local.datastore.repository.TodoRepositoryImplementation
import com.example.tasktodo.data.local.room.repository.TodoRoomRepository
import com.example.tasktodo.data.remote.repository.RemoteRepositoryImplementation
import com.example.tasktodo.domain.repository.RemoteRepository
import com.example.tasktodo.domain.repository.TodoRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindTodoRepository(todoRepositoryImplementation: TodoRoomRepository): TodoRepository

    @Binds
    @Singleton
    abstract fun bindRemoteRepository(remoteRepositoryImplementation: RemoteRepositoryImplementation): RemoteRepository
}