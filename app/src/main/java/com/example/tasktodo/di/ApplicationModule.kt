package com.example.tasktodo.di

import android.content.Context
import androidx.room.Room
import com.example.tasktodo.BuildConfig
import com.example.tasktodo.data.local.datastore.ToDoDataStore
import com.example.tasktodo.data.local.room.dao.TodoDao
import com.example.tasktodo.data.local.room.database.TodoDatabase
import com.example.tasktodo.data.remote.api.PullTodoApi
import com.example.tasktodo.data.remote.api.PushTodoApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Provides
    @Singleton
    fun provideTodoDataStore(
        @ApplicationContext context: Context
    ): ToDoDataStore {
        return ToDoDataStore(context = context)
    }

    @Provides
    @Singleton
    fun provideTodoDatabase(
        @ApplicationContext context: Context
    ): TodoDatabase {
        return Room.databaseBuilder(context, TodoDatabase::class.java, "Todo Database").build()
    }

    @Provides
    @Singleton
    fun provideToDoDao(
        database: TodoDatabase
    ): TodoDao {
        return database.todoDao
    }

    //Gson
    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().setLenient().create()

    //Retrofit
    @Provides
    @Singleton
    fun providePushRetrofit(
        gson: Gson
    ): PushTodoApi {
        return Retrofit.Builder()
            .baseUrl("https://todo.requestcatcher.com/")
            .client(
                OkHttpClient.Builder().also {
                    if (BuildConfig.DEBUG) {
                        val logging = HttpLoggingInterceptor()
                        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                        it.addInterceptor(logging)
                        it.connectTimeout(120, TimeUnit.SECONDS)
                        it.readTimeout(120, TimeUnit.SECONDS)
                        it.protocols(Collections.singletonList(Protocol.HTTP_1_1))
                    }
                }.build()
            )
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(PushTodoApi::class.java)
    }

    @Provides
    @Singleton
    fun providePullRetrofit(
        gson: Gson
    ): PullTodoApi {
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .client(
                OkHttpClient.Builder().also {
                    if (BuildConfig.DEBUG) {
                        val logging = HttpLoggingInterceptor()
                        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                        it.addInterceptor(logging)
                        it.connectTimeout(120, TimeUnit.SECONDS)
                        it.readTimeout(120, TimeUnit.SECONDS)
                        it.protocols(Collections.singletonList(Protocol.HTTP_1_1))
                    }
                }.build()
            )
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(PullTodoApi::class.java)
    }

}