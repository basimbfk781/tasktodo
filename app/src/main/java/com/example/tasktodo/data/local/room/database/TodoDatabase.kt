package com.example.tasktodo.data.local.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.tasktodo.data.local.room.dao.TodoDao
import com.example.tasktodo.domain.model.ToDo

@Database(
    entities = [ToDo::class],
    version = 1
)
abstract class TodoDatabase: RoomDatabase() {

    abstract val todoDao: TodoDao

}