package com.example.tasktodo.data.local.room.repository

import com.example.tasktodo.data.local.room.dao.TodoDao
import com.example.tasktodo.domain.model.ToDo
import com.example.tasktodo.domain.repository.TodoRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TodoRoomRepository @Inject constructor(
    private val todoDao: TodoDao
): TodoRepository {

    override fun getAllTodos(): Flow<List<ToDo>> {
        return todoDao.getAllToDo()
    }

    override suspend fun addTodos(list: List<ToDo>) {
        return todoDao.insertTodo(list)
    }

    override suspend fun updateCompleted(value: Boolean, todoItem: ToDo, index: Int) {
        todoDao.updateTodo(todoItem)
    }

}