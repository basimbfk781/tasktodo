package com.example.tasktodo.data.remote.api

import com.example.tasktodo.domain.model.ToDo
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PushTodoApi {

    @FormUrlEncoded
    @POST("test")
    suspend fun pushAllTodos(
        @Field("todos[]") list: List<ToDo>
    ): String

}

//https://todo.requestcatcher.com/test