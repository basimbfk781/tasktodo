package com.example.tasktodo.data.local.datastore.repository

import com.example.tasktodo.TodoItem
import com.example.tasktodo.data.local.datastore.ToDoDataStore
import com.example.tasktodo.domain.model.ToDo
import com.example.tasktodo.domain.repository.TodoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class TodoRepositoryImplementation @Inject constructor(
    private val todoDataStore: ToDoDataStore,
): TodoRepository {

    override fun getAllTodos(): Flow<List<ToDo>> {
        return todoDataStore.getAllTodos().map {
            it.map { toDoItem ->
                ToDo(
                    todoTitle = toDoItem.todoTitle,
                    todoDescription = toDoItem.todoDescription,
                    completed = toDoItem.completed
                )
            }
        }
    }

    override suspend fun addTodos(list: List<ToDo>) {
        todoDataStore.updateTodos(list.map { it.toTodoItem() })
    }

    override suspend fun updateCompleted(value: Boolean, todoItem: ToDo, index: Int) {
        todoDataStore.updateCompleted(value, todoItem.toTodoItem(), index)
    }

}