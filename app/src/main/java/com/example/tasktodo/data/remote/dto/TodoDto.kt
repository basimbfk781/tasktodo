package com.example.tasktodo.data.remote.dto

import com.example.tasktodo.domain.model.ToDo

data class TodoDto(
    val userId: Int,
    val id: Int,
    val title: String,
    val completed: Boolean
) {
    fun toToDo(): ToDo = ToDo(todoTitle = title, todoDescription = "No description from the server", completed = completed)
}
