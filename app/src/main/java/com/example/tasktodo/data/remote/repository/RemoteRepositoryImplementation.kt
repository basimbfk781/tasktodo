package com.example.tasktodo.data.remote.repository

import com.example.tasktodo.data.remote.api.PullTodoApi
import com.example.tasktodo.data.remote.api.PushTodoApi
import com.example.tasktodo.domain.model.ToDo
import com.example.tasktodo.domain.repository.RemoteRepository
import javax.inject.Inject

class RemoteRepositoryImplementation @Inject constructor (
    private val pullTodoApi: PullTodoApi,
    private val pushTodoApi: PushTodoApi
): RemoteRepository {
    override suspend fun pushTodos(list: List<ToDo>): String {
       return pushTodoApi.pushAllTodos(list)
    }

    override suspend fun pullTodos(): List<ToDo> {
        return pullTodoApi.getAllTodos().map { it.toToDo() }
    }
}