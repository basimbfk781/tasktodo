package com.example.tasktodo.data.remote.api

import com.example.tasktodo.data.remote.dto.TodoDto
import retrofit2.http.GET

interface PullTodoApi {

    @GET("todos")
    suspend fun getAllTodos(): List<TodoDto>

}

//https://jsonplaceholder.typicode.com/todos