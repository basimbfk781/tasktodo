package com.example.tasktodo.data.local.datastore

import android.content.Context
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.dataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.tasktodo.TodoItem
import com.example.tasktodo.Todos
import com.example.tasktodo.domain.model.ToDo
import com.google.protobuf.InvalidProtocolBufferException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.io.InputStream
import java.io.OutputStream

object ToDoItemSerializer: Serializer<Todos> {
    override val defaultValue: Todos
        get() = Todos.getDefaultInstance()

    override suspend fun readFrom(input: InputStream): Todos {
        try {
            return Todos.parseFrom(input)
        } catch (e: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read the proto" , e)
        }
    }

    override suspend fun writeTo(t: Todos, output: OutputStream) {
        t.writeTo(output)
    }

}

private val Context.todoPreferenceDataStore: DataStore<Todos> by dataStore(
    fileName = "todoitem.pb",
    serializer = ToDoItemSerializer
)

class ToDoDataStore(private val context: Context){

    suspend fun updateTodos(todos: List<TodoItem>) {
        context.todoPreferenceDataStore.updateData {
            it.toBuilder()
                .clearTodos()
                .addAllTodos(todos)
                .build()
        }
    }

    fun getAllTodos(): Flow<List<TodoItem>> {
        return context.todoPreferenceDataStore.data.map {
            it.todosList
        }
    }

    suspend fun updateCompleted(value: Boolean, todoItem: TodoItem, index: Int) {
         context.todoPreferenceDataStore.updateData {
            it.toBuilder()
                .setTodos(index, todoItem.toBuilder().setCompleted(value))
                .build()
        }
    }

}